const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  orderId: Number,
  products: Array,
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()},
  deletedAt: {type: Date, default: null},
});

schema.plugin(AutoIncrement, {inc_field: 'orderDetailId'});
module.exports = mongoose.model('orderDetails', schema);
