const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  productName: String,
  price: String,
  stock: String,
  featureImage: String,
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()},
  deletedAt: {type: Date, default: null},
});

schema.plugin(AutoIncrement, {inc_field: 'productId'});
module.exports = mongoose.model('products', schema);
