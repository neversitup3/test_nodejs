const mongoose = require('mongoose');

const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  userId: Number,
  totalPrice: Number,
  isCancle: {type: Boolean, default: false},
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()},
  deletedAt: {type: Date, default: null},
});

schema.plugin(AutoIncrement, {inc_field: 'orderId'});
module.exports = mongoose.model('orders', schema);
