const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()},
  deletedAt: {type: Date, default: null},
});

schema.plugin(AutoIncrement, {inc_field: 'userId'});
module.exports = mongoose.model('Users', schema);
