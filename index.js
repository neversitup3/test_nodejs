const app = require("express")();
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");
const fileUpload = require("express-fileupload");
const path = require("path");


app.use(express.static(__dirname + "/uploaded"));
app
  .use(
    cors({
      origin: true,
    })
  )
  .use(
    bodyParser.urlencoded({
      limit: "50mb",
      extended: true,
    })
  )
  .use(
    bodyParser.json({
      limit: "50mb",
    })
  )
  .use(
    express.json({
      limit: "50mb",
    })
  )
  .use(
    express.urlencoded({
      limit: "50mb",
      extended: true,
    })
  )
  .use(
    fileUpload({
      useTempFiles: true,
      tempFileDir: path.join(__dirname, "/tmp"),
    })
  );

const server = http.createServer(app);
app.use("/api", require("./Api"));


const port = 8081;
server.listen(port, () => {
  console.log(`Server is running on port : ${8081}`);
});
