const express = require("express");
const router = express.Router();
const Users = require("./../../models/userSchema");
var bcrypt = require("bcryptjs");
const jwt = require("./jwt");
const randtoken = require("rand-token"); 
const refreshTokens = {};

router.post("/login", async (req, res) => {

  const doc = await Users.findOne({ "email": req.body.email });

  if (doc) {
    isValidPassword = await bcrypt.compare(req.body.password, doc.password);
    if (isValidPassword) {
      const payload = {
        id: doc._id,
        email: doc.email,
        userId:doc.userId,
        firstName:doc.firstName,
        lastName:doc.lastName,
      };

      const token = jwt.sign(payload, "7200000000"); //2 hour

      return res.json({token});
    }else{
      res.status(401).json({});
    }
  } else {
    res.status(401).json({});
  }
});

router.post("/register", async (req, res) => {

  const {
    firstName,
    lastName,
    email,
    password
  } = req.body;

  let checkEmail = await Users.findOne({ email: email }).count();

  if(!checkEmail) {
    let hashPassword = await bcrypt.hash(password, 8);

    await Users.create({
      firstName : firstName,
      lastName: lastName,
      email: email,
      password: hashPassword
    });
  
    return res.json({});
  }else{
    return res.status(409).json({});
  }

});

module.exports = router;
