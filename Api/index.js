const express = require('express');
const router = express.Router();
const jwt = require('./auth/jwt');
require('./../config/db');

router.use(require('./auth/auth'));

router.use(jwt.verify);

router.use(require('./userManagement'));
router.use(require('./productManagement'));
router.use(require('./orderManagement'));

module.exports = router;
