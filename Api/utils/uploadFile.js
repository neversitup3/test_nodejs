const OBS = require('esdk-obs-nodejs');
const uuidv4 = require('uuid');
const path = require('path');
const fs = require('fs-extra');

const obsClient = new OBS({
  access_key_id: 'UTPJ3T65IFVEULDVLMGO',
  secret_access_key: 'YRFqIyvRUSw2kPk4czAwrHPW6G0LRn8b9mghox0Q',
  server: 'obs.ap-southeast-2.myhuaweicloud.com'
});

module.exports = {
  uploadFile:async function (files, folderName) {
    if (files != null) {
      let fileExtention = files.name.split('.').pop();
      let newFileName = `${uuidv4.v4()}.${fileExtention}`;
      let newpath = path.resolve(__dirname + `./../../uploaded/${folderName}`) + '/' + newFileName;

      await fs.move(files.tempFilePath, newpath);
      await obsClient.putObject({
        Bucket: 'topfan',
        Key: `uploaded/${folderName}/${newFileName}`,
        SourceFile: newpath
      }, (err, result) => {
        if (err) throw err;
      });

      return newFileName;
    }
  }
}
