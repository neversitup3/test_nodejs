const express = require("express");
const jwt_decode = require("jwt-decode");
const Users = require("./../models/userSchema");
const Order = require("./../models/orderSchema");

const router = express.Router();

router.get("/profile", async (req, res)=>{
  let token = req.headers.authorization.split(" ")[1];
  let decodeToken = jwt_decode(token);

  let profile = await Users.findOne({ userId: decodeToken.userId });

  let newData = {
    _id: profile._id,
    firstName: profile.firstName,
    lastName: profile.lastName,
    email: profile.email,
  }

  return res.json(newData);
});


router.get("/profile/myorder", async (req, res)=>{
  let token = req.headers.authorization.split(" ")[1];
  let decodeToken = jwt_decode(token);

  let myOrders = await Order.aggregate([
    { $match: { userId: parseInt(decodeToken.userId) } },
    { $lookup:
       {
         from: 'orderdetails',
         localField: 'orderId',
         foreignField: 'orderId',
         as: 'orderdetails'
       }
     }
  ]);

  return res.json({myOrders});
});


module.exports = router;
