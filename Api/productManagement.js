const express = require("express");
const router = express.Router();
const Product = require("./../models/productSchema");

router.get("/products", async (req, res)=>{
  const products = await Product.find();
  res.json({ products });
});

router.get("/products/:id", async (req, res)=>{

  const {
    id
  } = req.params;

  const product = await Product.findOne({productId: id});

  res.json(product);
});

router.get("/fake-product-data", async (req, res)=>{

  for (let index = 0; index < 10; index++) {
    await Product.create({
      productName: 'ข้าวหอมมะลิ', 
      price: '200',
      stock: '10',
      featureImage: '/2021/10/09/featureimage.jpg'
    }) 
  }

  res.json({});
});

module.exports = router;
