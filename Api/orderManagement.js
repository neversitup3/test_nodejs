const express = require("express");
const router = express.Router();
const Order = require("./../models/orderSchema");
const OrderDetail = require("./../models/orderDetailSchema");
const Product = require("./../models/productSchema");
const jwt_decode = require("jwt-decode");
const { count } = require("console");

router.post("/purchase", async (req, res)=>{
  const {
    userId,
    products
  } = req.body;

  let createOrder = await Order.create({userId: userId});
  let newOrderDetail = [];
  let total = 0;

  for (let index = 0; index < products.length; index++) {
    let productDetail = await Product.findOne({productId: products[index].id});
    newOrderDetail.push({
      products: 
      { 
        productId: productDetail.productId,
        productName: productDetail.productName, 
        price: productDetail.price, 
        featureImage: productDetail.featureImage,
        count: products[index].count
      },
      orderId: createOrder.orderId
    });

    let detailprice = parseInt(productDetail.price) * products[index].count;
    total += detailprice;

    await Product.updateOne({productId: products[index].id}, { stock: parseInt(productDetail.stock) -1 }); //delete stock
  }

  await Order.updateOne({ orderId: createOrder.orderId },{ totalPrice: total });
  await OrderDetail.insertMany(newOrderDetail);

  return res.json({});

});

router.get("/purchase/:id", async (req, res)=>{
  const {
    id,
  } = req.params;

  let data = await Order.aggregate([

    { $match: { orderId: parseInt(id) } },
    { $lookup:
       {
         from: 'orderdetails',
         localField: 'orderId',
         foreignField: 'orderId',
         as: 'orderdetails'
       }
     }
  ]);

  return res.json({data});

});

router.get("/toggle-cancle-order/:id", async (req, res)=>{

  const {
    id
  } = req.params;
  //check permission for cancle order

  let token = req.headers.authorization.split(" ")[1];
  let decodeToken = jwt_decode(token);

  let getOrder = await Order.findOne({ orderId: id });
  let toggleResult = getOrder.isCancle ? false : true;

  if(getOrder.userId == decodeToken.userId) {
    await Order.updateOne({ orderId: id },{ isCancle: toggleResult });
    return res.json({});
  }else{
    return res.status(403).json({});
  }
})



module.exports = router;
