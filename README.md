# Neversitup Test

This is my test code nodejs & mongodb (mongoose) . i start at 2 pm and end at 6 pm. including writing api doc

## Installation

Use yarn

```bash
yarn 
```
Use npm

```bash
npm i 
```

how to run this project ?
```bash
node index.js
```
this default project running on port 8081
## Authentication

#### How to register ?

METHOD : POST

Request Body
```bash
{
    "firstName": "ชื่อจริง",
    "lastName": "นามสกุล",
    "email": "test@test.com",
    "password": "1234"
}
```
URL
```bash
http://localhost:8081/api/register
```

Response status code

```bash
# 200 : Register success
{}
#409 : Conflict (email has already in systems )
{}
```
#### How to Login ?

METHOD : POST

Request Body
```bash
{
    "email": "test@test.com",
    "password" : "1234"
}
```
URL
```bash
http://localhost:8081/api/login
```

Response status code

```bash
# 200 : success
{
    "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzc1NjcxLCJleHAiOjE2NDA5NzU2NzEsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.NQy2NpOECzy3wr_XmG_olhnU5EjKe84_qt5Pqb4Y83UX4HeivcaYelSW6w-lO95APQVY4g4GEUq1q3wbECEwF2YfBPCIyQMJ-dg_Cna1v1EAG8529QQfrrhDbTjBP1lXcSU_tzywHA4xfaWGZa4H8PontBiDyiGsR5Hxmhi-8kA"
}
# 401 : Unauthorized (email or password is invalid)
{}
```

## User Management

#### How to get my profile ?

METHOD : GET

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```
URL
```bash
http://localhost:8081/api/profile
```

Response status code

```bash
# 200 : Login success
{
    "_id": "616140fcb24bc75a80249c99",
    "firstName": "ทัศน์ชัย",
    "lastName": "ศรีเจริญ",
    "email": "testsaaa@test.com"
}
```

#### How to get my order history ?

METHOD : GET

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```
URL
```bash
http://localhost:8081/api/profile/myorder
```

Response status code

```bash
# 200 : success
{
    "myOrders": [
        {
            "_id": "61616716cce3239c579259c6",
            "userId": 4,
            "isCancle": false,
            "createdAt": "2021-10-09T09:55:05.131Z",
            "updatedAt": "2021-10-09T09:55:05.131Z",
            "deletedAt": null,
            "orderId": 12,
            "__v": 0,
            "totalPrice": 3800,
            "orderdetails": [
                {
                    "_id": "61616716cce3239c579259d1",
                    "orderId": 12,
                    "products": [
                        {
                            "productId": 1,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 10
                        }
                    ],
                    "createdAt": "2021-10-09T09:55:05.133Z",
                    "updatedAt": "2021-10-09T09:55:05.133Z",
                    "deletedAt": null,
                    "__v": 0
                },
                {
                    "_id": "61616716cce3239c579259d2",
                    "orderId": 12,
                    "products": [
                        {
                            "productId": 2,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 4
                        }
                    ],
                    "createdAt": "2021-10-09T09:55:05.133Z",
                    "updatedAt": "2021-10-09T09:55:05.133Z",
                    "deletedAt": null,
                    "__v": 0
                },
                {
                    "_id": "61616716cce3239c579259d3",
                    "orderId": 12,
                    "products": [
                        {
                            "productId": 3,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 5
                        }
                    ],
                    "createdAt": "2021-10-09T09:55:05.133Z",
                    "updatedAt": "2021-10-09T09:55:05.133Z",
                    "deletedAt": null,
                    "__v": 0
                }
            ]
        }
    ]
}
```

## Product Management

you can fake product data 
run this route with bearer header token >> http://localhost:8081/api/fake-product-data

#### How to get all products ?

METHOD : GET

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```


URL
```bash
http://localhost:8081/api/products
```

Response status code

```bash
# 200 : success
{
    "products": [
        {
            "_id": "61614e2c486757e209cd278b",
            "productName": "ข้าวหอมมะลิ",
            "price": "200",
            "stock": "2",
            "featureImage": "/2021/10/09/featureimage.jpg",
            "createdAt": "2021-10-09T08:09:07.253Z",
            "updatedAt": "2021-10-09T08:09:07.253Z",
            "deletedAt": null,
            "productId": 1,
            "__v": 0
        },
        {
            "_id": "61614e2d486757e209cd278e",
            "productName": "ข้าวหอมมะลิ",
            "price": "200",
            "stock": "2",
            "featureImage": "/2021/10/09/featureimage.jpg",
            "createdAt": "2021-10-09T08:09:07.253Z",
            "updatedAt": "2021-10-09T08:09:07.253Z",
            "deletedAt": null,
            "productId": 2,
            "__v": 0
        },
        {
            "_id": "61614e2d486757e209cd2792",
            "productName": "ข้าวหอมมะลิ",
            "price": "200",
            "stock": "2",
            "featureImage": "/2021/10/09/featureimage.jpg",
            "createdAt": "2021-10-09T08:09:07.253Z",
            "updatedAt": "2021-10-09T08:09:07.253Z",
            "deletedAt": null,
            "productId": 3,
            "__v": 0
        }]
```

#### How to get one product ?

METHOD : GET

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```


URL
```bash
http://localhost:8081/api/products/:id
```

Response status code

```bash
# 200 : success
{
    "_id": "61614e2d486757e209cd278e",
    "productName": "ข้าวหอมมะลิ",
    "price": "200",
    "stock": "2",
    "featureImage": "/2021/10/09/featureimage.jpg",
    "createdAt": "2021-10-09T08:09:07.253Z",
    "updatedAt": "2021-10-09T08:09:07.253Z",
    "deletedAt": null,
    "productId": 2,
    "__v": 0
}
```

## Order Management

#### How to create order ?

METHOD : POST

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```

Request body
```bash
{
    "userId": 4,
    "products": [
        {
            "id": 1,
            "count": 10
        },
        {
            "id": 2,
            "count": 4
        },
        {
            "id": 3,
            "count": 5
        }
    ]
}
```

URL
```bash
http://localhost:8081/api/purchase
```

Response status code

```bash
# 200 : success
{}
```

#### How to cancle order ?

METHOD : GET

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```

URL
```bash
http://localhost:8081/api/toggle-cancle-order/:id
```

Response status code

```bash
# 200 : success
{}
```

#### How to get user order ?

METHOD : GET

Headers
```bash
{
    "Content-Type": "application/json",
    "Authorization" : "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjE0MGZjYjI0YmM3NWE4MDI0OWM5OSIsImVtYWlsIjoidGVzdHNhYWFAdGVzdC5jb20iLCJ1c2VySWQiOjQsImZpcnN0TmFtZSI6IuC4l-C4seC4qOC4meC5jOC4iuC4seC4oiIsImxhc3ROYW1lIjoi4Lio4Lij4Li14LmA4LiI4Lij4Li04LiNIiwiaWF0IjoxNjMzNzY1Mzc1LCJleHAiOjE2NDA5NjUzNzUsImF1ZCI6Imh0dHBzOi8vbmV2ZXJzaXR1cC5jb20iLCJpc3MiOiJOZXJ2ZXJTaXRVcCIsInN1YiI6Im1pbmR0YXRjaGFpMzRAZ21haWwuY29tIn0.W-Df_b3eqDJQ51LImPlXl9VhraPZoYTUU4Rz3eDMmr9MhjpqLRSsY_c7MQmL_gIKXsripcABcA_70Z9PMQhXD_MPLcT8rG-j1EugSLMYJo0EI8kJ2UHZY-2Hc8MHQXRZ6yWg87ar2lBIJ0N4siyAUcZ6xooAO36ualNMVtWa3xU"
}

# Authorization is Bearer and token in response on your login
```

URL
```bash
http://localhost:8081/api/profile/myorder
```

Response status code

```bash
# 200 : success
{
    "myOrders": [
        {
            "_id": "61616716cce3239c579259c6",
            "userId": 4,
            "isCancle": false,
            "createdAt": "2021-10-09T09:55:05.131Z",
            "updatedAt": "2021-10-09T09:55:05.131Z",
            "deletedAt": null,
            "orderId": 12,
            "__v": 0,
            "totalPrice": 3800,
            "orderdetails": [
                {
                    "_id": "61616716cce3239c579259d1",
                    "orderId": 12,
                    "products": [
                        {
                            "productId": 1,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 10
                        }
                    ],
                    "createdAt": "2021-10-09T09:55:05.133Z",
                    "updatedAt": "2021-10-09T09:55:05.133Z",
                    "deletedAt": null,
                    "__v": 0
                },
                {
                    "_id": "61616716cce3239c579259d2",
                    "orderId": 12,
                    "products": [
                        {
                            "productId": 2,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 4
                        }
                    ],
                    "createdAt": "2021-10-09T09:55:05.133Z",
                    "updatedAt": "2021-10-09T09:55:05.133Z",
                    "deletedAt": null,
                    "__v": 0
                },
                {
                    "_id": "61616716cce3239c579259d3",
                    "orderId": 12,
                    "products": [
                        {
                            "productId": 3,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 5
                        }
                    ],
                    "createdAt": "2021-10-09T09:55:05.133Z",
                    "updatedAt": "2021-10-09T09:55:05.133Z",
                    "deletedAt": null,
                    "__v": 0
                }
            ]
        },
        {
            "_id": "616168fe5b97172ae177cc91",
            "userId": 4,
            "isCancle": false,
            "createdAt": "2021-10-09T10:03:05.866Z",
            "updatedAt": "2021-10-09T10:03:05.866Z",
            "deletedAt": null,
            "orderId": 13,
            "__v": 0,
            "totalPrice": 3800,
            "orderdetails": [
                {
                    "_id": "616168fe5b97172ae177cc9c",
                    "orderId": 13,
                    "products": [
                        {
                            "productId": 1,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 10
                        }
                    ],
                    "createdAt": "2021-10-09T10:03:05.870Z",
                    "updatedAt": "2021-10-09T10:03:05.870Z",
                    "deletedAt": null,
                    "__v": 0
                },
                {
                    "_id": "616168fe5b97172ae177cc9d",
                    "orderId": 13,
                    "products": [
                        {
                            "productId": 2,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 4
                        }
                    ],
                    "createdAt": "2021-10-09T10:03:05.870Z",
                    "updatedAt": "2021-10-09T10:03:05.870Z",
                    "deletedAt": null,
                    "__v": 0
                },
                {
                    "_id": "616168fe5b97172ae177cc9e",
                    "orderId": 13,
                    "products": [
                        {
                            "productId": 3,
                            "productName": "ข้าวหอมมะลิ",
                            "price": "200",
                            "featureImage": "/2021/10/09/featureimage.jpg",
                            "count": 5
                        }
                    ],
                    "createdAt": "2021-10-09T10:03:05.870Z",
                    "updatedAt": "2021-10-09T10:03:05.870Z",
                    "deletedAt": null,
                    "__v": 0
                }
            ]
        }
    ]
}
```

## License
[Mind Tatchai](https://www.facebook.com/mind.tatchai)